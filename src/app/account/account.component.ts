import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { User } from '../entity/user';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  user:User;
  constructor(private authService:AuthService) { }

  ngOnInit() {
    //Pour utiliser le user dans l'application, on utilise le Subject
    //qu'on a défini dans le AuthService et on se subscribe dessus,
    this.authService.user.subscribe(user => this.user = user);
  }

}
