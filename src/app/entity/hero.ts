export interface Hero {
    id?:number;
    name:string;
    title?:string;
    birthdate?:Date;
    level:number;
}
