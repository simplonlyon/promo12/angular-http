import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth.guard';
import { LandingComponent } from './landing/landing.component';
import { TokenGuard } from './token.guard';
import { AccountComponent } from './account/account.component';


const routes: Routes = [
  //Pour guarder une route, on indique la guard dans le canActivate
  //de la route à protéger
  {path:"", component: HomeComponent, canActivate: [AuthGuard]},
  {path:"account", component: AccountComponent, canActivate: [AuthGuard]},
  {path:"register", component: RegisterComponent},
  {path:"landing", component: LandingComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
