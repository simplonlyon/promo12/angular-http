import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { User } from '../entity/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: User = {
    email: null,
    password: null
  };
  repeat: string = null;
  message:string;

  constructor(private authRepo: AuthService) { }

  ngOnInit() {
    
  }

  register() {
    if (this.validForm()) {
      this.authRepo.addUser(this.user).subscribe(() => this.message = 'Successfully registered !',
      data => this.message = data.error.message);
    }else {
      this.message = 'Passwords does not match';
    }
  }

  validForm():boolean {
    return this.user.email && this.user.password && this.user.password === this.repeat;
  }

}
