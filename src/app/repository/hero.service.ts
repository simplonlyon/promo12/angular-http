import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hero } from '../entity/hero';

/**
 * Les services sont des classes qui contiendront des methodes utilitaire
 * qu'on souhaitera utiliser à plusieurs endroit de l'application.
 * Par exemple on pourrait faire un service Login qui gère les méthode
 * de login/logout afin de pouvoir les utiliser depuis n'importe quel
 * component de l'application.
 * Ici, on fait un service un peu sur le modèle des Repositories côté
 * back donc l'objectif sera de récupérer les données du serveur et qui
 * concentrera toutes les requêtes http vers le serveur symfony
 */

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private url = 'http://localhost:8000/api/hero';
  /**
   * Ici, on injecte le service HttpClient de Angular qui permet de 
   * faire des requêtes AJAX efficacement. Il nécessite d'avoir importer
   * dans le AppModule le HttpClientModule.
   */
  constructor(private http:HttpClient) { }
  /**
   * La méthode findAll va faire une requête en GET vers l'api hero
   * de notre application symfony et va récupérer un tableau d'instance
   * de Hero
   */
  findAll(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.url);
  }
  /**
   * Méthode qui va faire un post vers l'application symfony afin de
   * faire persister un nouveau héro dans la base de données du serveur
   * @param hero Le hero à ajouter à la base de données
   */
  add(hero:Hero): Observable<Hero> {
    return this.http.post<Hero>(this.url, hero);
  }
}
