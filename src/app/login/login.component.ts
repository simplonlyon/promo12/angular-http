import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { User } from '../entity/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username:string;
  password:string;
  
  message:string;
  currentUser:User;

  constructor(private repoAuth:AuthService, private router:Router) { }

  ngOnInit() {
    this.repoAuth.user.subscribe(user => this.currentUser = user);
  
  }

  login() {
    this.repoAuth.login(this.username, this.password).subscribe(
      () => this.router.navigate(['']),
      data => this.message = 'Authentication Error.'
    )
  }

  logout() {
    this.repoAuth.logout();
    this.message = '';
    this.router.navigate(['landing']);
  }
}
