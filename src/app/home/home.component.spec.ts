import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { of } from 'rxjs';
import { HeroService } from '../repository/hero.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  const heroService = jasmine.createSpyObj('HeroService', ['add', 'findAll']);
  let findAll = heroService.findAll.and.returnValue(of([
    {name: 'test 1', title: 'title 1', level: 10, birthdate: '2014-10-12'},
    {name: 'test 2', title: 'title 2', level: 20, birthdate: '2014-11-12'},
    {name: 'test 3', title: 'title 3', level: 30, birthdate: '2014-12-12'},
  ]));
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers: [
        {provide: HeroService, useValue: heroService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
