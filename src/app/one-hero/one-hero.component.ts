import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Hero } from '../entity/hero';

@Component({
  selector: 'app-one-hero',
  templateUrl: './one-hero.component.html',
  styleUrls: ['./one-hero.component.scss']
})
export class OneHeroComponent implements OnInit {
  @Input()
  hero:Hero;
  @Output()
  close = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  onClose() {
    this.close.emit();
  }

}
